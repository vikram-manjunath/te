import numpy
from random_utility import random_utility
from collections import namedtuple
from deferred_acceptance import deferred_acceptance
from joblib import Parallel, delayed

DASimContext = namedtuple(
    'DASimContext',
    [
        'num_docs',  # Number of doctors
        'top_tier',  # Number of doctors in the top tier (remaining are in the second/bottom teir)
        'num_hosps',  # Number of hospitals
        'doc_heuristic',  # Which heuristic to use for doctors
        'hosp_heuristic',  # Which heuristic to use for hospitals
        'doc_interview_cap',  # Cap on number of interviews for doctors
        'hosp_interview_cap',  # Cap on number of interviews for hospitals
        'alpha',  # If using Lee(2017)/Echenique et al.(2020) model, weight on common component
        'beta',  # Correlation parameter for preferences
        'gamma'  # Alignment parameter for preferences
    ]
)


# Properties of a single match
def count_matched(match):
    return len([d for h in match for d in h])


def interview_distribution(interview_match, max_interviews):
    """
    Returns a vector of length max_interviews: distribution[n] is the number of doctors who get n interviews
    """
    distribution = numpy.zeros(max_interviews + 1, dtype=int)
    for doc in range(len(interview_match)):
        distribution[len(interview_match[doc])] += 1
    return distribution


def count_blocking_pairs(match, sample: random_utility.RandomUtility):
    inverse_match = invert_match(match, sample.num_doc, sample.num_hosp)
    blocking_pairs = 0
    for d in range(sample.num_doc):
        for h in range(sample.num_hosp):
            if ((inverse_match[d] is None) or sample.utility_doc(d, h) > sample.utility_doc(d, inverse_match[d])) \
                    and ((not match[h]) or sample.utility_hosp(h, d) > sample.utility_hosp(h, min(match[h]))):
                blocking_pairs += 1
    return blocking_pairs


def count_prefer_doc(m1, m2, sample: random_utility.RandomUtility):
    """
    returns tuple: (# of doctors who prefer m1, # of doctors who prefer m2)
    """
    prefer_m1 = prefer_m2 = 0
    for d in range(sample.num_doc):
        if m1[d] != m2[d]:
            if (not m1[d] is None) and (m2[d] is None or sample.utility_doc(d, m1[d]) > sample.utility_doc(d, m2[d])):
                prefer_m1 += 1
            else:
                # Since m1[d] != m2[d], if m1[d] is None, then m2[d] isn't None.
                prefer_m2 += 1
    return prefer_m1, prefer_m2


def count_prefer_hosp(m1, m2, sample: random_utility.RandomUtility):
    """
    returns tuple: (# of hospitals that prefer m1, # of hospitals that prefer m2)
    """
    prefer_m1 = prefer_m2 = 0
    for h in range(len(m1)):
        if m1[h] != m2[h]:
            if len(m1[h]) and \
                    (m2[h] == set() or sample.utility_hosp(h, min(m1[h])) > sample.utility_hosp(h, min(m2[h]))):
                # This is really terrible: need to change m1[h] and m2[h] to lists so we don't have to use min()
                prefer_m1 += 1
            else:
                # Since m1[h] != m2[h], if m1[h] is (), then m2[h] isn't ().
                prefer_m2 += 1
    return prefer_m1, prefer_m2


def count_tiered_matches(match, iter_ctx):
    tier1_rate = tier2_rate = 0
    for h in range(len(match)):
        for d in match[h]:
            if d < iter_ctx.top_tier:
                tier1_rate += 1
            else:
                tier2_rate += 1
    return tier1_rate, tier2_rate


# Average statistics across samples
def average_interview_distributions(interview_distributions):
    return [numpy.mean([interview_distributions[i][j] for i in range(len(interview_distributions))]) for j
            in range(len(interview_distributions[0]))]


def average_matched(matches):
    return numpy.mean([count_matched(match) for match in matches])


# Manipulate match
def invert_match(match, num_doc, num_hosp):
    """
    Match is a vector of sets: match[h] is the set of doctors h is matched to. All are cardinality 0 or 1.
    Return a vector of hospitals where inverse_match[d] is the hospital d is matched to
    """
    inverse_match = [None for _ in range(num_doc)]
    for h in range(num_hosp):
        for d in match[h]:
            inverse_match[d] = h
    return inverse_match


# interview-DA and match-DA
def interview_and_match(iter_ctx, sample):
    # Run hospital proposing DA to figure out interviews
    hosps_that_interview_doc = deferred_acceptance.deferred_acceptance(range(iter_ctx.num_hosps),
                                                                       range(iter_ctx.num_docs),
                                                                       sample.hosp_choice_funcs,
                                                                       sample.doc_choice_funcs,
                                                                       iter_ctx.hosp_interview_cap,
                                                                       iter_ctx.doc_interview_cap)
    # Now run doctor proposing DA restricting proposals according to interview outcome
    match = deferred_acceptance.deferred_acceptance(range(iter_ctx.num_docs),
                                                    range(iter_ctx.num_hosps),
                                                    sample.doc_choice_funcs,
                                                    sample.hosp_choice_funcs,
                                                    1,
                                                    1,
                                                    hosps_that_interview_doc)
    num_blocking_pairs = count_blocking_pairs(match, sample)
    return hosps_that_interview_doc, match, num_blocking_pairs


# run different combinations of interview-DA and match-DA as needed
def full_process_given_sample(iter_ctx, sample):
    _, match, num_blocking = interview_and_match(iter_ctx, sample)
    return match, num_blocking


def full_process(iter_ctx):
    # draw a sample from the random utility model
    sample = random_utility.RandomUtility(iter_ctx.num_hosps, iter_ctx.num_docs, iter_ctx.alpha, iter_ctx.beta,
                                          iter_ctx.doc_heuristic, iter_ctx.hosp_heuristic,
                                          iter_ctx.gamma, iter_ctx.top_tier)
    return full_process_given_sample(iter_ctx, sample)


def rank_comparison_two_caps(iter_ctx: DASimContext, cap1, cap2):
    ctx1 = iter_ctx._replace(doc_interview_cap=cap1)
    ctx2 = iter_ctx._replace(doc_interview_cap=cap2)
    sample = random_utility.RandomUtility(iter_ctx.num_hosps, iter_ctx.num_docs, iter_ctx.alpha, iter_ctx.beta,
                                          iter_ctx.doc_heuristic, iter_ctx.hosp_heuristic,
                                          iter_ctx.gamma, iter_ctx.top_tier)
    _, match_c1, _ = interview_and_match(ctx1, sample)
    _, match_c2, _ = interview_and_match(ctx2, sample)
    match_c1_inv = invert_match(match_c1, iter_ctx.num_docs, iter_ctx.num_hosps)
    match_c2_inv = invert_match(match_c2, iter_ctx.num_docs, iter_ctx.num_hosps)
    rank_diff = []
    for doc in range(iter_ctx.num_docs):
        hosp1 = match_c1_inv[doc]
        hosp2 = match_c2_inv[doc]
        rank_diff.append(sample.rank_diff(doc, hosp1, hosp2))
    return rank_diff


def full_process_two_caps(iter_ctx: DASimContext, cap1, cap2):
    ctx1 = iter_ctx._replace(doc_interview_cap=cap1)
    ctx2 = iter_ctx._replace(doc_interview_cap=cap2)
    sample = random_utility.RandomUtility(iter_ctx.num_hosps, iter_ctx.num_docs, iter_ctx.alpha, iter_ctx.beta,
                                          iter_ctx.doc_heuristic, iter_ctx.hosp_heuristic,
                                          iter_ctx.gamma, iter_ctx.top_tier)

    i_match_c1, match_c1, num_blocking_pairs_c1 = interview_and_match(ctx1, sample)
    i_match_c2, match_c2, num_blocking_pairs_c2 = interview_and_match(ctx2, sample)

    blk_diff = num_blocking_pairs_c2 - num_blocking_pairs_c1
    dp_c1, dp_c2 = count_prefer_doc(invert_match(match_c1, iter_ctx.num_docs, iter_ctx.num_hosps),
                                    invert_match(match_c2, iter_ctx.num_docs, iter_ctx.num_hosps),
                                    sample)
    hp_c1, hp_c2 = count_prefer_hosp(match_c1, match_c2, sample)

    return i_match_c1, i_match_c2, dp_c1, dp_c2, hp_c1, hp_c2, blk_diff


def all_cap_combinations(low_cap, high_cap, iter_ctx):
    sample = random_utility.RandomUtility(iter_ctx.num_hosps, iter_ctx.num_docs, iter_ctx.alpha, iter_ctx.beta,
                                          iter_ctx.doc_heuristic, iter_ctx.hosp_heuristic,
                                          iter_ctx.gamma, iter_ctx.top_tier)

    blocking_pairs = []
    matched = []
    for doc_cap in range(low_cap, high_cap + 1):
        for hosp_cap in range(low_cap, high_cap + 1):
            iter_ctx = iter_ctx._replace(doc_interview_cap=doc_cap, hosp_interview_cap=hosp_cap)
            _, match, num_blocking = interview_and_match(iter_ctx, sample)
            blocking_pairs.append(num_blocking)
            matched.append(count_matched(match))

    return blocking_pairs, matched


def full_process_caps_tiered(iter_ctx: DASimContext, caps):
    sample = random_utility.RandomUtility(iter_ctx.num_hosps, iter_ctx.num_docs, iter_ctx.alpha, iter_ctx.beta,
                                          iter_ctx.doc_heuristic, iter_ctx.hosp_heuristic,
                                          iter_ctx.gamma, iter_ctx.top_tier)
    mrates_t1 = []
    mrates_t2 = []
    for cap in caps:
        _, match, _ = interview_and_match(iter_ctx._replace(doc_interview_cap=cap), sample)
        mrate_t1, mrate_t2 = count_tiered_matches(match, iter_ctx)
        mrates_t1.append(mrate_t1)
        mrates_t2.append(mrate_t2)

    return mrates_t1, mrates_t2


# parallelize simulation
def iterate_sim(num_trials, experiment):
    return Parallel(n_jobs=min(num_trials, 8))(delayed(experiment)() for _ in range(num_trials))


def iterate_sim_over_doc_caps(low_cap, high_cap, default_ctx: DASimContext, experiment):
    return Parallel(n_jobs=min(8, high_cap - low_cap + 1))(
        delayed(experiment)(default_ctx._replace(doc_interview_cap=cap)) for cap
        in range(low_cap, high_cap + 1))


# Digest result of parallel computation
def digest_full_process(results):
    final_matches, num_blocking = zip(*results)
    return final_matches, numpy.mean(num_blocking)


def digest_full_process_pointwise(results):
    return zip(*results)


def digest_process_pointwise(results, cap1, cap2):
    i_c1, i_c2, dp_c1, dp_c2, hp_c1, hp_c2, blk_diff = zip(*results)
    i_dist_c1 = average_interview_distributions([interview_distribution(interview_match, cap1)
                                                 for interview_match in i_c1])
    i_dist_c2 = average_interview_distributions([interview_distribution(interview_match, cap2)
                                                 for interview_match in i_c2])

    return i_dist_c1, i_dist_c2, dp_c1, dp_c2, hp_c1, hp_c2, blk_diff


def digest_rank_differences(results):
    rank_diffs_by_sample = zip(*results)
    rank_diffs_all = []
    for rank_diff in rank_diffs_by_sample:
        rank_diffs_all += rank_diff
    return rank_diffs_all


def digest_process_pointwise_tiered(results, caps):
    mrates_t1, mrates_t2 = zip(*results)
    ave_mrates_t1 = [numpy.average([mrates_t1[i][c] for i in range(len(results))]) for c in range(len(caps))]
    ave_mrates_t2 = [numpy.average([mrates_t2[i][c] for i in range(len(results))]) for c in range(len(caps))]

    return ave_mrates_t1, ave_mrates_t2

