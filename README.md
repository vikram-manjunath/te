# Interview Hoarding #

This code replicates the simulations in the paper. It is written in Python. 
To run it, make sure that you have [Python 3 installed](https://www.python.org/downloads/).



### Installation  ###
The packages needed to run these simulations are listed in `requirements.txt`. 
The easiest way to install them is to run `pip install -r requirements.txt`.

### Running the simulations ###

#### Generating Figures 1-6 ####
To run the script, just type `python main.py`.
However, this will run all the simulations in the paper. You will find the output
under the directory `out/`. For each of the simulations, this produces a `.csv` file as well as a 
graph in PNG format. 
To suppress specific simulations, comment out the corresponding lines, as indicated, in `main.py`.
All parameter values are set in `main.py` as well.

#### Generating Figures 8-12 ####
To produce these, first run the simulations to generate Figure 1b for the desired range of parameter values. Combine the
`.csv` files into the filename specified in the corresponding function in `plot_from_csv` and run said function from 
the Python console.
