from matplotlib import pyplot
import pandas
import numpy
import seaborn


def plot_alpha_beta_relation():
    x = numpy.linspace(0, 100, 101)
    y = x/(x + 2*numpy.pi)

    fig = pyplot.figure()
    ax = fig.add_subplot(1, 1, 1)
    pyplot.plot(x, y, 'r')
    pyplot.yticks([0.0, 0.25, 0.5, 0.75, 1.0])
    ax.set_xlabel('\u03b2')
    ax.set_ylabel('\u03b1 that leads to the same degree of correlation')

    pyplot.savefig(f"out/alpha_beta.png", dpi=600)
    pyplot.clf()


def plot_match_rates_for_multiple_alphas():
    df = pandas.read_csv(f"tex/SimulationResults/AlternativeAlphas.csv")
    df = df.melt('cap', var_name='\u03b1', value_name='match_rate')
    lp = seaborn.lineplot(data=df, x='cap', y='match_rate', hue='\u03b1', style='\u03b1')
    pyplot.xlabel("Doctor Interview Cap")
    pyplot.ylabel("Average Percentage of Positions Filled")
    lp.legend(loc="lower center", framealpha=1.0, title='\u03b1')
    pyplot.savefig(f"out/match_rates_alpha.png", dpi=600)
    pyplot.clf()


def plot_match_rates_for_multiple_beta_gammas():
    df = pandas.read_csv(f"tex/SimulationResults/AlternativeBetaGammas.csv")
    df = df.melt('cap', var_name='bg', value_name='match_rate')
    lp = seaborn.lineplot(data=df, x='cap', y='match_rate', hue='bg', style='bg')
    pyplot.xlabel("Doctor Interview Cap")
    pyplot.ylabel("Average Percentage of Positions Filled")
    lp.legend(loc="lower center", framealpha=1.0, title='(\u03b2, \u03b3)')
    pyplot.savefig(f"out/match_rates_beta_gamma.png", dpi=600)
    pyplot.clf()


def plot_match_rates_for_multiple_num_docs():
    df = pandas.read_csv(f"tex/SimulationResults/AlternativeDocNums.csv")
    df = df.melt('cap', var_name='num_docs', value_name='match_rate')
    lp = seaborn.lineplot(data=df, x='cap', y='match_rate', hue='num_docs', style='num_docs')
    pyplot.xlabel("Doctor Interview Cap")
    pyplot.ylabel("Average Percentage of Positions Filled")
    handles, labels = pyplot.gca().get_legend_handles_labels()
    lp.legend(loc="best", framealpha=1.0, title='Number of Doctors',
              handles=reversed(handles), labels=reversed(labels))
    pyplot.savefig(f"out/match_rates_num_docs.png", dpi=600)
    pyplot.clf()


def plot_match_rates_for_multiple_market_sizes():
    df = pandas.read_csv(f"tex/SimulationResults/AlternativeMarketSizes.csv")
    df = df.melt('cap', var_name='market_size', value_name='match_rate')
    lp = seaborn.lineplot(data=df, x='cap', y='match_rate', hue='market_size', style='market_size')
    pyplot.xlabel("Doctor Interview Cap")
    pyplot.ylabel("Average Percentage of Positions Filled")
    handles, labels = pyplot.gca().get_legend_handles_labels()
    lp.legend(loc="best", framealpha=1.0, title='(#Doctors, #Hospitals)')
    pyplot.savefig(f"out/match_rates_market_sizes.png", dpi=600)
    pyplot.clf()


def plot_match_rates_for_multiple_ls():
    df = pandas.read_csv(f"tex/SimulationResults/Alternativels.csv")
    df = df.melt('cap', var_name='hosp_cap', value_name='match_rate')
    lp = seaborn.lineplot(data=df, x='cap', y='match_rate', hue='hosp_cap', style='hosp_cap')
    pyplot.xlabel("Doctor Interview Cap")
    pyplot.ylabel("Average Percentage of Positions Filled")
    handles, labels = pyplot.gca().get_legend_handles_labels()
    lp.legend(loc="lower center", framealpha=1.0, title='l', handles=reversed(handles), labels=reversed(labels))
    pyplot.savefig(f"out/match_rates_ls.png", dpi=600)
    pyplot.clf()


def plot_match_rates_against_kminusl_varying_l():
    df = pandas.read_csv(f"tex/SimulationResults/kminusl.csv")
    df = df.melt('cap', var_name='hosp_cap', value_name='match_rate')
    lp = seaborn.lineplot(data=df, x='cap', y='match_rate', hue='hosp_cap', style='hosp_cap')
    pyplot.xlabel("Doctor Cap Minus Hospital Cap")
    pyplot.ylabel("Average Percentage of Positions Filled")
    handles, labels = pyplot.gca().get_legend_handles_labels()
    lp.legend(loc="lower center", framealpha=1.0, title='l', handles=reversed(handles), labels=reversed(labels))
    pyplot.savefig(f"out/kminusl.png", dpi=600)
    pyplot.clf()
