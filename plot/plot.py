from matplotlib import pyplot
import pandas
import numpy


def show_ave_matched(ave_matched, low_cap, num_hosps, fname_suffix=""):
    pyplot.plot([low_cap + i for i in range(len(ave_matched))],
                [100 * ave_matched[i] / num_hosps for i in range(len(ave_matched))])
    df = pandas.DataFrame({'cap': [low_cap + i for i in range(len(ave_matched))],
                           'match_rate': [100 * ave_matched[i] / num_hosps for i in range(len(ave_matched))]})
    df.to_csv(f'out/match_rates{fname_suffix}.csv', index=False, header=True)

    pyplot.xlabel("Resident Interview Cap")
    pyplot.ylabel("Average Percentage of Positions Filled")
    pyplot.savefig(f'out/match_rates{fname_suffix}.png', dpi=600)
    pyplot.clf()


def show_preference_two_caps(num_agents, cap1, cap2, prefer_c1, prefer_c2, num_trials, agents="Residents",
                             fname_suffix=""):
    cap_labels = [f'prefer_cap_{cap1}']
    display_labels = [f"Prefer match with cap of {cap1}"]
    fname_prefix = ""
    if cap2 is None:
        display_labels.append(f"Prefer unconstrained match")
        cap_labels.append(f'prefer_unconstrained')
        fname_prefix = "unconstrained_"
    else:
        display_labels.append(f"Prefer match with cap of {cap2}")
        cap_labels.append(f'prefer_cap_{cap2}')

    df = pandas.DataFrame({cap_labels[0]: prefer_c1, cap_labels[1]: prefer_c2})
    df.to_csv(f'out/{fname_prefix}cap_preference_{agents}{fname_suffix}.csv', index=True, index_label='trial_number',
              header=True)

    for column, color, label in zip(df.columns, ['teal', 'orange'], display_labels):
        pyplot.hist(df[column], color=color, label=label)

    pyplot.legend()

    pyplot.xlabel(f"Number of {agents} Out of {num_agents}")
    pyplot.ylabel(f"Occurrences Out of {num_trials} Trials")
    pyplot.savefig(f"out/{fname_prefix}cap_preferences_{agents}{fname_suffix}.png", dpi=600)
    pyplot.clf()


def show_blocking_diff_two_caps(cap1, cap2, num_trials, blocking_dff, fname_suffix=""):
    pyplot.plot()
    pyplot.hist(blocking_dff)
    pyplot.xlabel(f'Excess Blocking Pairs with Interview Cap {cap2} Compared  {cap1}')
    pyplot.ylabel(f'Occurrences Out of {num_trials} Trials')
    df = pandas.DataFrame({f'blocking_diff_{cap2}_vs_{cap2}': blocking_dff})
    df.to_csv(f'out/blocking_diff{fname_suffix}.csv', index=True, index_label='trial_number', header=True)
    pyplot.savefig(f'out/blocking_diff{fname_suffix}.png', dpi=600)
    pyplot.clf()


def show_interview_distributions(low_cap, high_cap, low_dist_raw, high_dist_raw, num_to_display, fname_suffix=""):
    # Display histogram showing the distribution for the cap corresponding to the lowest index
    # and the highest index cap

    # Truncate both distributions at num_to_display
    low_dist = low_dist_raw[:num_to_display + 1] if len(low_dist_raw) >= num_to_display \
        else low_dist_raw + [0] * (num_to_display - len(low_dist_raw) + 1)
    low_dist.append(sum(low_dist_raw) - sum(low_dist))

    high_dist = high_dist_raw[:num_to_display + 1] if len(high_dist_raw) >= num_to_display \
        else high_dist_raw + [0] * (num_to_display - len(high_dist_raw) + 1)
    high_dist.append(sum(high_dist_raw) - sum(high_dist))

    pyplot.subplots()
    n_groups = num_to_display + 2
    index = numpy.arange(n_groups)
    bar_width = 0.35

    bar1 = pyplot.barh(index, low_dist, bar_width, color='b', label=f'{low_cap}', alpha=0.5)
    bar2 = pyplot.barh(index + bar_width, high_dist, bar_width, color='g',
                       label=f'{high_cap}', alpha=0.5)

    tick_labels = [f"{i}" if i % 2 == 0 else "" for i in range(n_groups - 1)]
    tick_labels.append(f"≥ {num_to_display + 1}")
    pyplot.yticks(index, tick_labels)

    pyplot.ylabel("Number of Interviews")
    pyplot.xlabel("Average Number of Students")
    pyplot.legend((bar1, bar2), (f'Interview cap of {low_cap}', f'No interview cap'))
    pyplot.tight_layout()
    pyplot.savefig(f'out/interview_dist_truncated{fname_suffix}.png', dpi=600)
    pyplot.clf()

    adjusted_low_dist = low_dist_raw + [0] * (len(high_dist_raw) - len(low_dist_raw)) if \
        len(high_dist_raw) > len(low_dist_raw) else low_dist_raw
    adjusted_high_dist = high_dist_raw + [0] * (len(low_dist_raw) - len(high_dist_raw)) if \
        len(high_dist_raw) < len(low_dist_raw) else high_dist_raw

    df = pandas.DataFrame({f'ave_number_of_students_with_cap_{low_cap}': adjusted_low_dist,
                           f'ave_number_of_students_with_cap_{high_cap}': adjusted_high_dist})
    df.to_csv(f'out/interview_dist{fname_suffix}.csv', index=True, index_label='number_of_interviews', header=True)


def show_blocking_pairs(blocking_pairs, low_cap, fname_suffix=""):
    pyplot.plot([low_cap + i for i in range(len(blocking_pairs))], blocking_pairs)
    pyplot.xlabel("Resident Interview Cap")
    pyplot.ylabel("Average Number of Blocking Pairs")
    pyplot.savefig(f'out/blocking_pairs{fname_suffix}.png', dpi=600)
    pyplot.clf()
    df = pandas.DataFrame({'cap': [low_cap + i for i in range(len(blocking_pairs))],
                           'ave_number_of_blocking_pars': blocking_pairs})
    df.to_csv(f'out/blocking_pairs{fname_suffix}.csv', index=False, header=True)


def show_data_per_cap_pair(low_cap, high_cap, data, zlabel, fname_prefix, csv_label, elev, azim):
    fig = pyplot.figure()
    ax = fig.add_subplot(111, projection='3d')

    cap_range = list(range(low_cap, high_cap + 1))
    x = []
    y = []

    for cap in cap_range:
        x += cap_range
        y += [cap for _ in cap_range]

    ax.plot_trisurf(x, y, data, linewidth=1, cmap='summer', alpha=0.85, antialiased=True)
    ax.view_init(elev=elev, azim=azim)

    ax.set_xlabel('Hospital Interview Cap')
    ax.set_ylabel('Resident Interview Cap')
    ax.set_zlabel(zlabel)

    pyplot.savefig(f"out/{fname_prefix}_cap_variation_3d.png", dpi=600)
    pyplot.clf()

    df = pandas.DataFrame({'hosp_cap': x, 'doc_cap': y, f'{csv_label}': data})
    df.to_csv(f"out/{fname_prefix}_cap_variation.csv", index_label='trial_number', index=True, header=True)


def show_tiered_match_rates(ave_t1_rates, ave_t2_rates, caps, fname_suffix):
    labels = [f"{cap}" for cap in caps]
    width = 0.35
    fig, ax = pyplot.subplots()

    ax.bar(labels, ave_t2_rates, width, label="Filled by Bottom Tier")
    ax.bar(labels, ave_t1_rates, width, label="Filled by Top Tier", bottom=ave_t2_rates)

    ax.set_ylabel("Positions Filled")
    ax.set_xlabel("Resident Interview Capacity")
    ax.legend()
    pyplot.savefig(f"out/tiered_match_rate_comparison{fname_suffix}.png", dpi=600)
    pyplot.clf()

    df = pandas.DataFrame({'labels': [f'cap_{cap}' for cap in caps],
                           'tier1_rate': ave_t1_rates,
                           'tier2_rate': ave_t2_rates})
    df.to_csv(f"out/tiered_match_rate_comparison{fname_suffix}.csv", index=False, header=True)


def show_rank_difference_distribution(num_docs, num_trials, rank_diffs, cap1, cap2):
    pyplot.hist(rank_diffs, bins=100)
    pyplot.xlabel(f"Rank of match when k = {cap1} minus rank at k = {cap2}")
    pyplot.ylabel(f"Frequency out of {num_docs * num_trials} (= {num_docs} doctors x {num_trials} samples)")

    pyplot.savefig('out/rank_difference_distribution.png', dpi=600)
    pyplot.clf()
