"""
This module implements the random utility model of Hitsch, Hortascu, and Ariely (AER 2010) as adapted by
Ashlagi, Kanoria, and Leshno (JPE 2017)

An instance of RandomUtility has everything to represent an instance of a matching market:
- doctors
- hospitals
- a choice function for each doctor and hospital
"""
import random
import numpy.random
import choice_heuristics.top_choices


class RandomUtility:

    def __init__(self, num_hosp=1, num_doc=1, alpha=None, beta=0.0,
                 doc_heuristic=choice_heuristics.top_choices.simple_choice,
                 hosp_heuristic=choice_heuristics.top_choices.simple_choice,
                 gamma=0.0, top_tier=0):
        """
        If alpha is None, use Ashlagi, Kanoria, and Leshno (2017) model
        When beta and gamma are both equal to zero (default), preferences are drawn iid from the uniform distribution
        If alpha isn't None, use the Lee (2016) model

        The first top_tier docs are (almost) ensured to be better for each hospital than the remaining docs
        """
        self.num_hosp = num_hosp
        self.num_doc = num_doc
        self.alpha = 0.0
        self.beta = float(beta)
        self.gamma = float(gamma)
        self.ashlagi_model = alpha is None
        if not self.ashlagi_model:
            self.alpha = float(alpha)

        self.top_tier = top_tier
        # if modeling two tiered market, add tier_boost to top tier docs
        self.tier_boost = self.beta + self.gamma + 10.0 if self.ashlagi_model else 1.0
        # adding self.beta + self.gamma + 10 almost surely puts a top tier candidate above every lower tier candidate
        assert 0 < top_tier <= num_doc  # top tier cannot have more than the total number of doctors

        # Need to draw random variables xA[j]~U[0,1] and xD[j]~U[0,1] for each hospital j and each doctor j
        self.xA_hosp = [random.random() for _ in range(self.num_hosp)]
        self.xA_doc = [random.random() for _ in range(self.num_doc)]
        self.xD_hosp = [random.random() for _ in range(self.num_hosp)]
        self.xD_doc = [random.random() for _ in range(self.num_doc)]

        # Finally, need hospital/doctor specific epsilon drawn from the standard logistic distribution
        # epsilon_doc[i][j] is doctor i's epsilon for hospital j (epsilon_{ij})
        # epsilon_hosp[i][j] is hospital i's epsilon for doctor j (epsilon_{ij})
        if self.ashlagi_model:
            self.epsilon_doc = [[numpy.random.logistic() for _ in range(self.num_hosp)] for _ in range(self.num_doc)]
            self.epsilon_hosp = [[numpy.random.logistic() for _ in range(self.num_doc)] for _ in range(self.num_hosp)]
        else:
            self.epsilon_doc = [[random.random() for _ in range(self.num_hosp)] for _ in range(self.num_doc)]
            self.epsilon_hosp = [[random.random() for _ in range(self.num_doc)] for _ in range(self.num_hosp)]

        self.doc_choice_funcs = [lambda c, s, d=d: doc_heuristic(c, s, lambda x, d=d: self.utility_doc(d, x),
                                                                 0)  # No tiers for doctors' preferences
                                 for d in range(num_doc)]
        self.hosp_choice_funcs = [lambda c, s, h=h: hosp_heuristic(c, s, lambda x, h=h: self.utility_hosp(h, x),
                                                                   self.top_tier)
                                  for h in range(num_hosp)]

        # Now make ordinal preferences
        # THIS IS HELPFUL WHEN DEBUGGING: EASIER TO LOOK AT A RANKING
        # This isn't used anywhere else
        # Comment out when not in use: memory intensive
        # self.doc_ordinal_pref = [self.ordinalize_doc(i) for i in range(self.num_doc)]
        # self.hosp_ordinal_pref = [self.ordinalize_hosp(i) for i in range(self.num_hosp)]

    def utility_doc(self, doc, hosp):
        assert doc < self.num_doc and hosp < self.num_hosp
        if self.ashlagi_model:
            return self.beta * self.xA_hosp[hosp] - self.gamma*(self.xD_doc[doc]-self.xD_hosp[hosp])**2 + \
                   self.epsilon_doc[doc][hosp]
        else:
            return self.alpha * self.xA_hosp[hosp] + (1.0 - self.alpha) * self.epsilon_doc[doc][hosp]

    def utility_hosp(self, hosp, doc):
        assert doc < self.num_doc and hosp < self.num_hosp
        if self.ashlagi_model:
            utility = self.beta * self.xA_doc[doc] - self.gamma * (self.xD_hosp[hosp] - self.xD_doc[doc]) ** 2 + \
                      self.epsilon_hosp[hosp][doc]
        else:
            utility = self.alpha * self.xA_doc[doc] + (1.0 - self.alpha) * self.epsilon_hosp[hosp][doc]

        # There are self.top_tier docs in the top tier. So if the doc is less than self.top_tier, then add the boost
        if doc < self.top_tier:
            utility += self.tier_boost

        return utility

    def ordinalize_doc(self, doc):
        sorted_hosps = list(range(self.num_hosp))
        sorted_hosps.sort(key=lambda x: -self.utility_doc(doc, x))
        return sorted_hosps

    def ordinalize_hosp(self, hosp):
        sorted_docs = list(range(self.num_doc))
        sorted_docs.sort(key=lambda x: -self.utility_hosp(hosp, x))
        return sorted_docs

    def shock_utility_doc(self, doc, hosp):
        # result of utility shock is distributed according to standard logistic
        if self.ashlagi_model:
            self.epsilon_doc[doc][hosp] += numpy.random.logistic()
        else:
            self.epsilon_doc[doc][hosp] += random.random()

    def shock_utility_hosp(self, hosp, doc):
        # result of utility shock is distributed according to standard logistic
        if self.ashlagi_model:
            self.epsilon_hosp[hosp][doc] += numpy.random.logistic()
        else:
            self.epsilon_doc[doc][hosp] += random.random()

    def rank_diff(self, doc, hosp1, hosp2):
        h1_util = self.utility_doc(doc, hosp1) if hosp1 else float('-inf')
        h2_util = self.utility_doc(doc, hosp2) if hosp2 else float('-inf')

        sign = 1 if h1_util > h2_util else -1
        low_util = min(h1_util, h2_util)
        high_util = max(h1_util, h2_util)
        between = 0
        if low_util == high_util:
            return between
        for h in range(self.num_hosp):
            util = self.utility_doc(doc, h)
            if low_util < util < high_util:
                between += 1
        return sign*between





