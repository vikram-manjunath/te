"""
Given a set of options and a preference over them, which K to choose?

The simplest heuristic is to choose the 'best' K.

The parameters for a choice instance are the set of things to choose from, the utility function that
ranks these things, and the size of the top tier (which may be zero).

When choice_size is 1, all choice functions return the singleton containing the highest utility element
"""
import random


def choice_func(function):
    def wrapped(choice_set, choice_size, utility, top_tier):
        # In case of a singleton choice, return the best item
        if choice_size == 1 and len(choice_set):
            return {max(choice_set, key=lambda x: utility(x))}
        if len(choice_set) <= choice_size:
            return choice_set
        else:
            return function(choice_set, choice_size, utility, top_tier)

    return wrapped


@choice_func
def simple_choice(choice_set, choice_size, utility, top_tier=0):
    sorted_choice_set = list(choice_set)
    sorted_choice_set.sort(key=lambda x: -utility(x))
    return set(sorted_choice_set[:choice_size])


@choice_func
def choice_with_bottom_tier_safety(choice_set, choice_size, utility, top_tier):
    """
    choice_with_bottom_tier_safety is equivalent to simple_choice when top_tier = self.num_docs
    """
    # know that choice_set has at least two elements
    # Pick a random safety choice
    low_tier_docs = [d for d in choice_set if d >= top_tier]
    safety = set()
    if len(low_tier_docs):
        safety = set(random.choices(low_tier_docs, k=min(1, choice_size, len(low_tier_docs))))

    # Pick according to simple choice from the rest
    choice = simple_choice(choice_set - safety, choice_size - len(safety), utility, top_tier)

    return choice.union(safety)
