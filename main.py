from tools import *
from choice_heuristics import top_choices
import numpy
import time
import os
from plot import plot


def iterate_over_both_caps(num_trials, low_cap, high_cap):
    blocking, matched = digest_full_process_pointwise(iterate_sim(num_trials, lambda: all_cap_combinations(low_cap,
                                                                                                           high_cap,
                                                                                                           ctx)))
    blocking_pairs = []
    match_rates = []
    for i in range(len(blocking[0])):
        blocking_pairs.append(numpy.average([blocking_result[i] for blocking_result in blocking]))
        match_rates.append(numpy.average([match[i] for match in matched]) / ctx.num_hosps)

    plot.show_data_per_cap_pair(low_cap, high_cap, blocking_pairs, "Average Number of Blocking Pairs",
                                'blocking_pairs', 'blocking_pairs', 10, 75)
    plot.show_data_per_cap_pair(low_cap, high_cap, match_rates, "Average Match Rate", 'match_rate', 'match_rate', 45,
                                80)


def iterate_full_process_over_doc_interview_cap(num_trials, low_cap, high_cap, fname_suffix=""):
    ave_matched = []
    average_blocking_pairs = []
    for doc_interviews in range(low_cap, high_cap + 1):
        iter_ctx = ctx._replace(doc_interview_cap=doc_interviews)
        final_matches, num_blocking = digest_full_process(iterate_sim(num_trials, lambda: full_process(iter_ctx)))
        matched = average_matched(final_matches)
        ave_matched.append(matched)
        average_blocking_pairs.append(num_blocking)

    # Output average match rate and number of blocking pairs
    plot.show_ave_matched(ave_matched, low_cap, ctx.num_hosps, fname_suffix=fname_suffix)
    plot.show_blocking_pairs(average_blocking_pairs, low_cap, fname_suffix=fname_suffix)


def compare_two_doc_interview_caps(cap1, cap2, num_trials, num_to_display, fname_suffix=""):
    results = digest_process_pointwise(iterate_sim(num_trials, lambda: full_process_two_caps(ctx, cap1, cap2)),
                                       cap1, cap2)
    int_c1, int_c2, dp_c1, dp_c2, hp_c1, hp_c2, blk_dif = results
    plot.show_preference_two_caps(ctx.num_docs, cap1, cap2, dp_c1, dp_c2, num_trials, fname_suffix=fname_suffix)
    plot.show_preference_two_caps(ctx.num_hosps, cap1, cap2, hp_c1, hp_c2, num_trials, agents="Hospitals",
                                  fname_suffix=fname_suffix)
    plot.show_blocking_diff_two_caps(cap1, cap2, num_trials, blk_dif, fname_suffix=fname_suffix)
    plot.show_interview_distributions(cap1, cap2, int_c1, int_c2, num_to_display, fname_suffix=fname_suffix)


def compare_rank_differences_two_caps(cap1, cap2, num_trials):
    rank_diffs = digest_rank_differences(iterate_sim(num_trials, lambda: rank_comparison_two_caps(ctx, cap1, cap2)))
    plot.show_rank_difference_distribution(ctx.num_docs, num_trials, rank_diffs, cap1, cap2)


def compare_doc_interview_caps_tiered(num_trials, caps, fname_suffix=""):
    assert 0 < ctx.top_tier < ctx.num_docs
    t1_mrates, t2_mrates = digest_process_pointwise_tiered(iterate_sim(num_trials,
                                                                       lambda: full_process_caps_tiered(ctx, caps)),
                                                           caps)
    plot.show_tiered_match_rates(t1_mrates, t2_mrates, caps, fname_suffix=fname_suffix)


if __name__ == '__main__':
    # All output saved in directory 'out' so make sure it exists
    try:
        os.mkdir('out')
        print('Created output directory.')
    except FileExistsError:
        print('Output directory exists. Continuing with simulations.')

    # Only fiddle with parameters here
    n_trials = 100  # number of trials for each simulations
    ctx = DASimContext(
        num_docs=470,
        top_tier=470,
        num_hosps=400,
        doc_heuristic=top_choices.simple_choice,
        hosp_heuristic=top_choices.simple_choice,  # start with simple_choice and switch to safety later
        doc_interview_cap=None,
        hosp_interview_cap=25,
        alpha=None,  # set this to None to use the Ashlagi, Kanoria, and Leshno (2017) model with beta and gamma below
        beta=40,
        gamma=20.0
    )

    start_time = time.time()

    iterate_full_process_over_doc_interview_cap(num_trials=n_trials, low_cap=2, high_cap=100)  # Figures 1 a/b and 4a

    compare_two_doc_interview_caps(cap1=ctx.hosp_interview_cap, cap2=ctx.num_hosps,
                                   num_trials=n_trials, num_to_display=35)  # Figures 2 a to d

    iterate_over_both_caps(num_trials=n_trials, low_cap=20, high_cap=30)  # Figure 3

    compare_rank_differences_two_caps(cap1=ctx.hosp_interview_cap, cap2=ctx.num_hosps, num_trials=n_trials)

    ctx = ctx._replace(top_tier=200)
    compare_doc_interview_caps_tiered(num_trials=n_trials,
                                      caps=[5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70],
                                      fname_suffix="_simple")  # Figure 5a

    # Change choice functions and tier setting before tiered model
    ctx = ctx._replace(top_tier=200, hosp_heuristic=top_choices.choice_with_bottom_tier_safety)
    iterate_full_process_over_doc_interview_cap(num_trials=n_trials, low_cap=2, high_cap=100,
                                                fname_suffix="_tiered_safety")  # Figure 4b
    compare_doc_interview_caps_tiered(num_trials=n_trials,
                                      caps=[5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70],
                                      fname_suffix="_safety")  # Figure 5b
    compare_two_doc_interview_caps(cap1=ctx.hosp_interview_cap, cap2=ctx.num_hosps,
                                   num_trials=n_trials, num_to_display=35,
                                   fname_suffix="_tiered_safety")  # Figures 6 a to c

    end_time = time.time()
    print(f'Took {end_time - start_time}s')
