

def deferred_acceptance(proposers, acceptors, proposer_choice_funcs, acceptor_choice_funcs, proposer_cap, acceptor_cap,
                        can_propose_to_arg=None):
    """
    Gale-Shapley Deferred Acceptance in a many-to-many context.
    :param proposers: set of proposing agents
    :param acceptors: set of accepting agents (other side of the market from proposers)
    :param proposer_choice_funcs: proposer_choice_funcs[i] takes two arguments 'choice_set' and 'choice_size' and
    returns a subset of choice_set containing no more than choice_size elements. This reflects the choice function for
    the ith proposer. Note that there are no assumptions of substitutability here.
    :param acceptor_choice_funcs: acceptor_choice_funcs[i] takes two arguments 'choice_set' and 'choice_size' and
    returns a subset of choice_set containing no more than choice_size elements. This reflects the choice function for
    the ith acceptor. Note that there are no assumptions of substitutability here.
    :param proposer_cap: In the context of a many-to-many matching model, this is the most acceptors a proposer can be
    matched to.
    :param acceptor_cap: In the context of a many-to-many matching model, this is the most proposers an acceptor can be
    matched to.
    :param can_propose_to: Restrict proposals using this; if None, no restrictions
    :return: a list of sets m, such that m[i] is the set of proposers that acceptor i is matched to
    """
    # If no restriction on proposal, then set can_propose_to to all acceptors
    can_propose_to = None
    if can_propose_to_arg is None:
        can_propose_to = [set(acceptors) for _ in proposers]
    else:
        # Make a deep copy of can_propose_to_arg
        can_propose_to = [set(can_propose_to_arg[i]) for i in proposers]

    not_fully_held = set(proposers)
    tentatively_accepted = [set() for _ in acceptors]
    num_held = [0 for _ in proposers]

    while len(not_fully_held) > 0:
        # Start a new round of proposals
        # Copy tentatively accepted proposals to acceptor choice set
        acceptor_choice_sets = [tentatively_accepted[i].copy() for i in acceptors]

        this_round_proposals = [[] for _ in proposers]
        for proposer in not_fully_held:
            choice_set = can_propose_to[proposer].copy()
            choice_size = proposer_cap - num_held[proposer]
            this_round_proposals[proposer] = proposer_choice_funcs[proposer](choice_set, choice_size)

            # Assume that all of the proposals will be accepted: add all of this round's proposals to the number of
            # held proposals. We'll deduct these later if rejected.
            num_held[proposer] += len(this_round_proposals[proposer])
            # Add these proposals to acceptor's choice sets
            # Remove acceptor from set of acceptors that proposer can propose to
            for acceptor in this_round_proposals[proposer]:
                acceptor_choice_sets[acceptor].add(proposer)
                can_propose_to[proposer].discard(acceptor)

        # Once all proposals are made in this round, update tentatively accepted proposals
        for acceptor in acceptors:
            tentatively_accepted[acceptor] = set(acceptor_choice_funcs[acceptor](acceptor_choice_sets[acceptor],
                                                                                 acceptor_cap))
            # Free up availability of proposals for rejected proposers
            rejected = acceptor_choice_sets[acceptor].difference(tentatively_accepted[acceptor])
            for proposer in rejected:
                # Deduct the number of held proposals
                num_held[proposer] -= 1

        # Update the set of proposers who are not fully held
        # Start by clearing the set and rebuild it by checking each proposer
        not_fully_held.clear()
        for proposer in proposers:
            # A proposer is fully held if either proposer_cap proposals are held or there are no more proposals that
            # proposer can make
            if num_held[proposer] < proposer_cap and len(can_propose_to[proposer]) != 0:
                not_fully_held.add(proposer)
            else:
                not_fully_held.discard(proposer)

    return tentatively_accepted
