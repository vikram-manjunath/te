numpy==1.19.4
joblib~=1.0.0
matplotlib~=3.3.3
pandas~=1.2.0
seaborn~=0.11.1